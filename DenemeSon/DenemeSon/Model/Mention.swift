//
//  Mention.swift
//  DenemeSon
//
//  Created by Hakan Üstünbaş on 24.03.2021.
//

import Foundation

struct Mention {
    var userMention : String?
    var mentionImage : String?
    var mentionTime : String?
    var userLiked : Bool? = false
    var mentionOwner : Person?
}
