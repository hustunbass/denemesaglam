//
//  Reply.swift
//  DenemeSon
//
//  Created by Hakan Üstünbaş on 24.03.2021.
//

import Foundation

struct Reply {
    var userReply : String?
    var replyTime : String?
    var userReplied : Person?
    var mentionReply : Mention?
}
