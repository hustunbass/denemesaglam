//
//  TwoColumnViewCell.swift
//  DenemeSon
//
//  Created by Hakan Üstünbaş on 23.03.2021.
//

import UIKit

class TwoColumnViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userJob: UILabel!
    @IBOutlet weak var mentionLabel: UILabel!
    @IBOutlet weak var mentionImage: UIImageView!
    @IBOutlet weak var mentionDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.clipsToBounds = true
        backView.layer.cornerRadius = 10
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOffset = CGSize(width: 0.5, height: 4.0)
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowRadius = 5.0
    }

}
