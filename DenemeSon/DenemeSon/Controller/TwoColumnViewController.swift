//
//  TwoColumnViewController.swift
//  DenemeSon
//
//  Created by Hakan Üstünbaş on 23.03.2021.
//

import UIKit
import Kingfisher

class TwoColumnViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
  
    let service = Service()
    var mentions = [Mention]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.register(UINib(nibName:"TwoColumnViewCell", bundle: nil), forCellWithReuseIdentifier: "twoColumnCell")
        mentions = service.createMentions()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mentions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "twoColumnCell", for: indexPath) as! TwoColumnViewCell
        cell.mentionLabel.text = mentions[indexPath.row].userMention
        cell.userName.text = mentions[indexPath.row].mentionOwner?.userName
        cell.mentionImage.kf.setImage(with: URL(string: mentions[indexPath.row].mentionImage!))
        cell.mentionDate.text = mentions[indexPath.row].mentionTime
        cell.userJob.text = mentions[indexPath.row].mentionOwner?.userJob
        
        return cell
    }
}
