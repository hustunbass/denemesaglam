//
//  Person.swift
//  DenemeSon
//
//  Created by Hakan Üstünbaş on 24.03.2021.
//

import Foundation

struct Person {
    var profileImage : String?
    var userName : String?
    var userJob : String?
}
